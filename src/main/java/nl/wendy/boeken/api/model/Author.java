package nl.wendy.boeken.api.model;

import nl.wendy.boeken.api.dao.ConnectionManager;
import nl.wendy.boeken.api.exceptions.DatabaseException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static nl.wendy.boeken.api.dao.DatabaseConnectable.cleanUp;

/**
 * class that holds information on an author
 */
public class Author {
    private int id;
    private String firstName = null;
    private String lastName = null;

    /**
     * constructor
     * @param id the id of the author in the database
     * @param firstName first name of the author
     * @param lastName last name of the author
     */
    public Author(int id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    /**
     * constructor
     * @param id the id of the author in the database
     * @throws DatabaseException if database connectivity goes wrong
     */
    public Author(int id) throws DatabaseException {
        this.id = id;
        extractName();
    }

    /**
     * Extract name of author from database using the id of the author
     * @throws DatabaseException if database connectivity goes wrong
     */
    private void extractName() throws DatabaseException {
        ResultSet rs = null;
        PreparedStatement stmt = null;
        try {
            Connection conn = ConnectionManager.defaultManager().getConnection();
            String sql = "select VOORNAAM, ACHTERNAAM from auteurs where AUTEURNR = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, this.id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                this.firstName = rs.getString("VOORNAAM");
                this.lastName = rs.getString("ACHTERNAAM");
            } else {
                throw new DatabaseException("Author: extractName - No results returned for author id " + this.id);
            }
        } catch (SQLException e) {
            throw new DatabaseException("Author: extractName - SQLException: " + e.getMessage());
        } finally {
            cleanUp(stmt, rs, true);
        }
    }

    // getters
    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFullName() {
        return this.firstName + " " + this.lastName;
    }
}
