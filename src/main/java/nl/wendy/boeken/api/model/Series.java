package nl.wendy.boeken.api.model;

import nl.wendy.boeken.api.dao.ConnectionManager;
import nl.wendy.boeken.api.exceptions.DatabaseException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static nl.wendy.boeken.api.dao.DatabaseConnectable.cleanUp;

/**
 * Class that holds information on a book series
 */
public class Series {
    private int seriesNr;
    private Author author;
    private String name;
    private boolean complete;
    private boolean tooBoring;
    private boolean read;

    /**
     * constructor
     * @param seriesNr the id of the series in the database
     * @param author the author who wrote the series
     * @param name the name of the series
     * @param complete whether I have all the books of the series in the database
     * @param tooBoring whether it was too boring and I didn't finish it
     * @param read whether I read it.
     */
    public Series(int seriesNr, Author author, String name, boolean complete, boolean tooBoring, boolean read) {
        this.seriesNr = seriesNr;
        this.author = author;
        this.name = name;
        this.complete = complete;
        this.tooBoring = tooBoring;
        this.read = read;
    }

    /**
     * constructor
     * @param seriesNr the id of the series in the database
     * @throws DatabaseException if database connectivity goes wrong
     */
    public Series(int seriesNr) throws DatabaseException {
        this.seriesNr = seriesNr;
        extractSeriesInfo();
    }

    /**
     * collect all info on the series from the database using the series id
     * @throws DatabaseException if database connectivity goes wrong
     */
    private void extractSeriesInfo() throws DatabaseException {
        ResultSet rs = null;
        PreparedStatement stmt = null;
        try {
            Connection conn = ConnectionManager.defaultManager().getConnection();
            String sql = "select * from series where SERIENR = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, this.seriesNr);
            rs = stmt.executeQuery();
            if (rs.next()) {
                this.name = rs.getString("SERIE");
                this.complete = rs.getBoolean("COMPLEET");
                this.tooBoring = rs.getBoolean("TOO_BORING");
                this.read = rs.getBoolean("GELEZEN");
                this.author = new Author(rs.getInt("AUTEURNR"));
            } else {
                throw new DatabaseException("Series: extractSeriesInfo - No results returned for series id " + this.seriesNr);
            }
        } catch (SQLException e) {
            throw new DatabaseException("Series: extractSeriesInfo - SQLException: " + e.getMessage());
        } finally {
            cleanUp(stmt, rs, true);
        }
    }

    // getters and setters
    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public boolean isTooBoring() {
        return tooBoring;
    }

    public void setTooBoring(boolean tooBoring) {
        this.tooBoring = tooBoring;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public int getSeriesNr() {
        return seriesNr;
    }

    public Author getAuthor() {
        return author;
    }

    public String getName() {
        return name;
    }
}
