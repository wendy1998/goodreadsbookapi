package nl.wendy.boeken.api.dao;

import nl.wendy.boeken.api.exceptions.DatabaseException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static nl.wendy.boeken.api.dao.DatabaseConnectable.cleanUp;

/**
 * Class that manages database connectivity.
 */
public class ConnectionManager {
	private static ConnectionManager singletonInstance = null;

	/**
	 * Private constructor so only singletonInstance can be used.
	 */
	private ConnectionManager() {};

	/**
	 * Make sure only one instance of this class exists in this program.
	 * @return The single instance of this class
	 */
	public static ConnectionManager defaultManager() {
		if (singletonInstance == null) {
			singletonInstance = new ConnectionManager();
		}
		return singletonInstance;
	}

	/**
	 * Create database connection.
	 * @return an SQL Connection object with the specified database
	 * @throws DatabaseException, when connection cannot be made.
	 */
	public Connection getConnection() throws DatabaseException {
		try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            return DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/bibliotheek?useUnicode=true&" +
							"characterEncoding=UTF-8&" +
							"useJDBCCompliantTimezoneShift=true&" +
							"useLegacyDatetimeCode=false&serverTimezone=EST",
					"dev", "development");
        } catch (SQLException e) {
        	throw new DatabaseException("GetConnection - SQLException: " + e.getMessage());
        } catch (ClassNotFoundException e) {
        	throw new DatabaseException("GetConnection - ClassNotFound: " + e.getMessage());
		}
	}

	/**
	 * Executes an update statement in the database
	 * @param sql the statement
	 * @return the number of rows updated
	 * @throws DatabaseException if database connectivity goes wrong
	 */
	public int executeUpdateStatement(String sql) throws DatabaseException {
		PreparedStatement stmt = null;
		int numRowsAffected = 0;
		try {
			Connection conn = ConnectionManager.defaultManager().getConnection();
			stmt = conn.prepareStatement(sql);
			numRowsAffected = stmt.executeUpdate();
		} catch (SQLException e) {
			throw new DatabaseException("ConnectionManager: executeUpdateStatement - SQLException: " + e.getMessage());
		} finally {
			cleanUp(stmt, null, true);
		}

		return numRowsAffected;
	}
}
