package nl.wendy.boeken.api.logging;

import nl.wendy.boeken.api.books.Book;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * takes care of logging everything that happens in the program
 */
public class Logger {
    String type;
    HashMap<String, HashMap<Book, Book>> conversions = setUpConversionsMap();
    ArrayList<Exception> exceptionsLog = new ArrayList<>();

    /**
     * constructor
     * @param type the type of logger
     */
    public Logger(String type) {
        this.type = type;
    }

    /**
     * Log an exception
     * @param e the exception to log
     */
    public void logException(Exception e) {
        this.exceptionsLog.add(e);
    }

    /**
     * initialize hashmap where conversions are logged
     * @return the hashmap
     */
    private HashMap<String, HashMap<Book, Book>> setUpConversionsMap() {
        HashMap<String, HashMap<Book, Book>> out = new HashMap<>(2);
        out.put("released_book", new HashMap<>());
        out.put("not_released", new HashMap<>());

        return out;
    }

    /**
     * write all the errors logged to the provided file.
     * @param filePath the file
     * @throws IOException if something goes wrong
     */
    public void writeErrorsToFile(String filePath) throws IOException {
        File log = new File(filePath);
        //noinspection ResultOfMethodCallIgnored
        log.createNewFile();
        try (BufferedWriter writer = Files.newBufferedWriter(log.toPath(), StandardOpenOption.CREATE)) {
            for (Exception e: this.exceptionsLog) {
                writer.write(e.toString());
                writer.newLine();
                writer.write(Arrays.toString(e.getStackTrace()).replaceAll(", ", ",\n"));
                writer.newLine();
                writer.newLine();
            }
        }
    }

    /**
     * Add a conversion to the conversions log
     * @param book1 book that was converted
     * @param book2 book it was converted into
     */
    public void logConversion(Book book1, Book book2) {
        HashMap<Book, Book> conversionsMap = this.conversions.get(book1.getType());
        if (conversionsMap.containsKey(book1)) {
            conversionsMap.replace(book1, book2);
        } else {
            conversionsMap.put(book1, book2);
        }
    }

    // getters
    public String getType() {
        return type;
    }

    public HashMap<String, HashMap<Book, Book>> getConversions() {
        return conversions;
    }
}
