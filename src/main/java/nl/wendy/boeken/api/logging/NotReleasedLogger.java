package nl.wendy.boeken.api.logging;

import nl.wendy.boeken.api.books.NotReleasedBook;
import nl.wendy.boeken.api.searching.SearchResult;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * takes care of logging searches of not released books
 */
public class NotReleasedLogger extends Logger {
    public HashMap<NotReleasedBook, ArrayList<SearchResult>> searchResults = new HashMap<>();

    /**
     * constructor
     */
    public NotReleasedLogger() {
        super("not_released");
    }

    // getters
    public HashMap<NotReleasedBook, ArrayList<SearchResult>> getSearchResults() {
        return searchResults;
    }
}
