package nl.wendy.boeken.api.books;

import nl.wendy.boeken.api.model.Author;
import nl.wendy.boeken.api.model.Series;

public class OwnedBook extends Book {
    int bookNr;
    boolean stillCalibre = true;
    boolean stillEReader = true;
    boolean stillCollection = true;
    boolean novel68 = false;
    boolean thirdPartyBook = false;
    boolean read = false;
    String bookType = "Ebook";

    /**
     * constructor
     * @param author the author of the book
     * @param series the series the book is in (can be null)
     * @param numberInSeries the place this book has in its series (can be null)
     * @param title the title of the book (can be null if book is unpublished)
     * @param language the language the book was written in
     * @param bookNr the id of the book in the database
     * @param stillCalibre whether it needs to be added to calibre
     * @param stillEReader whether it needs to be added to my ereader
     * @param stillCollection whether it needs to be added to a collection on my ereader
     * @param novel68 whether it was found on novel68
     * @param bookType ebook or normal book
     */
    public OwnedBook(Author author,
                     Series series,
                     String numberInSeries,
                     String title,
                     String language,
                     int bookNr,
                     boolean stillCalibre,
                     boolean stillEReader,
                     boolean stillCollection,
                     boolean novel68,
                     String bookType) {
        super("released_book", author, series, numberInSeries, title, language);
        this.bookNr = bookNr;
        this.stillCalibre = stillCalibre;
        this.stillEReader = stillEReader;
        this.stillCollection = stillCollection;
        this.novel68 = novel68;
        this.bookType = bookType;
    }

    /**
     * constructor
     * @param author the author of the book
     * @param series the series the book is in (can be null)
     * @param numberInSeries the place this book has in its series (can be null)
     * @param title the title of the book (can be null if book is unpublished)
     * @param language the language the book was written in
     * @param bookNr the id of the book in the database
     * @param stillCalibre whether it needs to be added to calibre
     * @param stillEReader whether it needs to be added to my ereader
     * @param stillCollection whether it needs to be added to a collection on my ereader
     * @param novel68 whether it was found on novel68
     * @param thirdPartyBook whether it is for a third party
     * @param read whether I have read the book
     * @param bookType ebook or normal book
     */
    public OwnedBook(Author author,
                     Series series,
                     String numberInSeries,
                     String title,
                     String language,
                     int bookNr,
                     boolean stillCalibre,
                     boolean stillEReader,
                     boolean stillCollection,
                     boolean novel68,
                     boolean thirdPartyBook,
                     boolean read,
                     String bookType) {
        super("released_book", author, series, numberInSeries, title, language);
        this.bookNr = bookNr;
        this.stillCalibre = stillCalibre;
        this.stillEReader = stillEReader;
        this.stillCollection = stillCollection;
        this.novel68 = novel68;
        this.thirdPartyBook = thirdPartyBook;
        this.read = read;
        this.bookType = bookType;
    }

    /**
     * constructor
     * @param author the author of the book
     * @param series the series the book is in (can be null)
     * @param numberInSeries the place this book has in its series (can be null)
     * @param title the title of the book (can be null if book is unpublished)
     * @param language the language the book was written in
     * @param bookNr the id of the book in the database
     * @param stillCalibre whether it needs to be added to calibre
     * @param stillEReader whether it needs to be added to my ereader
     * @param stillCollection whether it needs to be added to a collection on my ereader
     * @param novel68 whether it was found on novel68
     */
    public OwnedBook(Author author,
                     Series series,
                     String numberInSeries,
                     String title,
                     String language,
                     int bookNr,
                     boolean stillCalibre,
                     boolean stillEReader,
                     boolean stillCollection,
                     boolean novel68) {
        super("released_book", author, series, numberInSeries, title, language);
        this.bookNr = bookNr;
        this.stillCalibre = stillCalibre;
        this.stillEReader = stillEReader;
        this.stillCollection = stillCollection;
        this.novel68 = novel68;
    }

    /**
     * constructor
     * @param author the author of the book
     * @param series the series the book is in (can be null)
     * @param numberInSeries the place this book has in its series (can be null)
     * @param title the title of the book (can be null if book is unpublished)
     * @param language the language the book was written in
     * @param bookNr the id of the book in the database
     */
    public OwnedBook(Author author, Series series, String numberInSeries, String title, String language, int bookNr) {
        super("released_book", author, series, numberInSeries, title, language);
        this.bookNr = bookNr;
    }

    // getters and setters
    public boolean isStillCalibre() {
        return stillCalibre;
    }

    public void setStillCalibre(boolean stillCalibre) {
        this.stillCalibre = stillCalibre;
    }

    public boolean isStillEReader() {
        return stillEReader;
    }

    public void setStillEReader(boolean stillEReader) {
        this.stillEReader = stillEReader;
    }

    public boolean isStillCollection() {
        return stillCollection;
    }

    public void setStillCollection(boolean stillCollection) {
        this.stillCollection = stillCollection;
    }

    public boolean isNovel68() {
        return novel68;
    }

    public void setNovel68(boolean novel68) {
        this.novel68 = novel68;
    }
}
