package nl.wendy.boeken.api.books;

import nl.wendy.boeken.api.model.Author;
import nl.wendy.boeken.api.model.Series;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class WantedBook extends Book {
    private int tableId;
    private Date dateAdded;
    private boolean hopeless;

    /**
     * constructor
     * @param author the author of the book
     * @param series the series the book is in (can be null)
     * @param numberInSeries the place this book has in its series (can be null)
     * @param title the title of the book (can be null if book is unpublished)
     * @param language the language the book was written in
     * @param tableId the id of the book in the database
     * @param dateAdded the date it was added to the wanted table
     * @param hopeless whether it is hopeless and there is no use in searching for it
     */
    public WantedBook(Author author,
                      Series series,
                      String numberInSeries,
                      String title,
                      String language,
                      int tableId,
                      String dateAdded,
                      boolean hopeless) {
        super("released_book", author, series, numberInSeries, title, language);
        this.tableId = tableId;
        this.dateAdded = setDateAdded(dateAdded);
        this.hopeless = hopeless;
    }

    /**
     * constructor
     * @param author the author of the book
     * @param series the series the book is in (can be null)
     * @param numberInSeries the place this book has in its series (can be null)
     * @param title the title of the book (can be null if book is unpublished)
     * @param language the language the book was written in
     * @param tableId the id of the book in the database
     */
    public WantedBook(Author author, Series series, String numberInSeries, String title, String language, int tableId) {
        super("released_book", author, series, numberInSeries, title, language);
        this.tableId = tableId;
        this.dateAdded = Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
        this.hopeless = false;
    }

    // getters and setters
    public int getTableId() { return tableId; }

    public void setTableId(int tableId) { this.tableId = tableId; }

    public Date getDateAdded() {
        return dateAdded;
    }

    private Date setDateAdded(String dateAdded) {
        if (!dateAdded.equals("NA")) {
            return Date.from(LocalDate.parse(dateAdded, DateTimeFormatter.ofPattern("yyyy-MM-dd"))
                    .atStartOfDay(ZoneId.systemDefault()).toInstant());
        }
        return null;
    }

    public boolean isHopeless() {
        return hopeless;
    }

    public void setHopeless(boolean hopeless) {
        this.hopeless = hopeless;
    }
}
