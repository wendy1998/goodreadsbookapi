package nl.wendy.boeken.api.books;

import nl.wendy.boeken.api.model.Author;
import nl.wendy.boeken.api.model.Series;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * class that models a book that wasn't released yet
 */
public class NotReleasedBook extends Book {
    private String releaseDate = null;
    private boolean stillAgenda;
    private int tableId;

    /**
     * constructor
     * @param author the author of the book
     * @param series the series the book is in (can be null)
     * @param numberInSeries the place this book has in its series (can be null)
     * @param title the title of the book (can be null)
     * @param language the language the book was written in
     * @param releaseDate the date the book can be released on (can be null)
     * @param stillAgenda whether it needs to be added to the agenda
     * @param tableId the id the book has in the database
     */
    public NotReleasedBook(Author author,
                           Series series,
                           String numberInSeries,
                           String title,
                           String language,
                           String releaseDate,
                           boolean stillAgenda,
                           int tableId) {
        super("not_released", author, series, numberInSeries, title, language);
        setReleaseDate(releaseDate);
        setStillAgenda(stillAgenda);
        this.tableId = tableId;
    }

    /**
     * whether all the information is present for this book
     * @return true or false
     */
    public boolean isComplete() {
        return releaseDate != null &&
                releaseDate.matches("^\\d{4}-\\d{2}-\\d{2}")
                && this.title != null;
    }

    /**
     * whether the releasedate has passed
     * @return true or false
     */
    public boolean wasReleased() {
        if (isComplete()) {
            LocalDate parsed = LocalDate.parse(this.releaseDate, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            LocalDate now = LocalDate.now(ZoneId.systemDefault());
            return parsed.isBefore(now);
        }
        return false;
    }

    @Override
    public String toString() {
        String toFormat = "%s %s - ";
        if (this.series != null) {
            toFormat = toFormat + "%s";
            if (this.numberInSeries != null) {
                toFormat = toFormat + " %s: %s";
                return String.format(toFormat, this.author.getFirstName(),
                        this.author.getLastName(), this.series.getName(),
                        this.numberInSeries, this.title);
            } else {
                toFormat = toFormat + ": %s";
                return String.format(toFormat, this.author.getFirstName(),
                        this.author.getLastName(), this.series.getName(), this.title);
            }
        } else {
            toFormat = toFormat + "%s";
            return String.format(toFormat, this.author.getFirstName(),
                    this.author.getLastName(), this.title);
        }
    }

    /**
     * construct a release date from years, months and days
     * @param year the year
     * @param month the month
     * @param day the day
     * @return a string combining all elements that weren't null
     */
    public String constructReleaseDate(Integer year, Integer month, Integer day) {
        StringBuilder builder = new StringBuilder();
        if (year != null) {
            builder.append(year);
            if (month != null) {
                builder.append("-");
                if (String.valueOf(month).length() == 1) {
                    builder.append("0");
                }
                builder.append(month);
                if (day != null) {
                    builder.append("-");
                    if (String.valueOf(day).length() == 1) {
                        builder.append("0");
                    }
                    builder.append(day);
                }
            }
        } else {
            return null;
        }
        return builder.toString();
    }

    // getters and setters
    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        if (releaseDate != null && (releaseDate.matches("^\\d{4}-\\d{2}-\\d{2}") ||
                releaseDate.matches("^\\d{4}-\\d{2}") ||
                releaseDate.matches("^\\d{4}"))) {
            this.releaseDate = releaseDate;
        }

    }

    public boolean isStillAgenda() {
        return stillAgenda;
    }

    public void setStillAgenda(boolean stillAgenda) {
        this.stillAgenda = stillAgenda;
    }

    public void setStillAgenda(int stillAgenda) {
        this.stillAgenda = stillAgenda > 0;
    }

    public int getTableId() {
        return tableId;
    }

    public void setTableId(int tableId) {
        this.tableId = tableId;
    }
}
