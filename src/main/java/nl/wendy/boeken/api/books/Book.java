package nl.wendy.boeken.api.books;

import nl.wendy.boeken.api.model.Author;
import nl.wendy.boeken.api.model.Series;

/**
 * Class that stores everything of a book
 */
public class Book {
    String type;
    Author author;
    String language;
    Series series;
    String numberInSeries;
    String title;

    /**
     * constructor
     * @param type the type of book -> wanted, owned or not released
     * @param author the author of the book
     * @param series the series the book is in (can be null)
     * @param numberInSeries the place this book has in its series (can be null)
     * @param title the title of the book (can be null if book is unpublished)
     * @param language the language the book was written in
     */
    public Book(String type, Author author, Series series, String numberInSeries, String title, String language) {
        this.type = type;
        this.author = author;
        this.series = series;
        this.numberInSeries = numberInSeries;
        this.title = setTitle(title);
        this.language = language;
    }

    // setters
    public String setTitle(String title) {
        if (title != null && !title.equals("NA")) {
            return title.replaceAll("\\(.+\\)", "").trim();
        }
        return null;
    }

    // getters
    public String getType() {
        return type;
    }

    public Author getAuthor() {
        return author;
    }

    public Series getSeries() {
        return series;
    }

    public String getNumberInSeries() {
        return numberInSeries;
    }

    public String getTitle() {
        return title;
    }

    public String getLanguage() {
        return language;
    }
}
