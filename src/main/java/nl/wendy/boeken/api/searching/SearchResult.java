package nl.wendy.boeken.api.searching;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

/**
 * Class that stores results of searches
 */
public class SearchResult {
    String method;
    Boolean found = true;
    String title = null;
    Integer publicationMonth = null;
    Integer publicationDay = null;
    Integer publicationYear = null;

    /**
     * constructor
     * @param method either full search or title search
     * @param data the json representation of the data found in the search.
     */
    public SearchResult(String method, JSONObject data) {
        this.method = method;
        parseJson(data);
    }

    /**
     * parse all the information from the provided data
     * @param data the provided data
     * @throws JSONException if parsing goes wrong.
     */
    private void parseJson(JSONObject data) throws JSONException {
        try {
            if (method.contains("title")) {
                if (data.has("book")) {
                    JSONObject book = data.optJSONObject("book");
                    if (book != null) {
                        if (book.has("title")) {
                            if (!Objects.equals(book.optString("title"), "")) {
                                this.title = book.getString("title");
                            }
                        }
                        if (book.optJSONObject("work") != null) {
                            ExtractPublicationDate(book.optJSONObject("work"));
                        }
                    } else {
                        found = false;
                    }
                } else {
                    found = false;
                }
            } else {
                if (data.has("search")) {
                    JSONObject results = data.getJSONObject("search").optJSONObject("results");
                    if (results != null) {
                        if (results.optJSONObject("work") != null) {
                            JSONObject work = results.optJSONObject("work");
                            ExtractPublicationDate(work);
                            JSONObject bestBook = work.optJSONObject("best_book");
                            if (bestBook != null) {
                                if (!bestBook.optString("title").equals("")) {
                                    this.title = bestBook.getString("title");
                                }
                            }
                        } else {
                            found = false;
                        }
                    } else {
                        found = false;
                    }
                } else {
                    found = false;
                }
            }
        } catch (JSONException e) {
            found = false;
            throw e;
        }
    }

    /**
     * extract full date from the three date parts in the data
     * @param work the json data the date needs to be extracted from.
     */
    private void ExtractPublicationDate(JSONObject work) {
        JSONObject pubDay = work.optJSONObject("original_publication_day");
        if (pubDay != null) {
            if (pubDay.optInt("content") > 0) {
                this.publicationDay = pubDay.optInt("content");
            }
        }

        JSONObject pubMonth = work.optJSONObject("original_publication_month");
        if (pubMonth != null) {
            if (pubMonth.optInt("content") > 0) {
                this.publicationMonth = pubMonth.optInt("content");
            }
        }

        JSONObject pubYear = work.optJSONObject("original_publication_year");
        if (pubYear != null) {
            if (pubYear.optInt("content") > 0) {
                this.publicationYear = pubYear.optInt("content");
            }
        }
    }

    @Override
    public String toString() {
        return "SearchResult{" +
                "method=" + method +
                ", found=" + found +
                ", title=" + title  +
                ", publicationDay=" + publicationDay +
                ", publicationMonth=" + publicationMonth +
                ", publicationYear=" + publicationYear +
                '}';
    }

    // getters

    public String getMethod() {
        return method;
    }

    public Boolean getFound() {
        return found;
    }

    public String getTitle() {
        return title;
    }

    public Integer getPublicationMonth() {
        return publicationMonth;
    }

    public Integer getPublicationDay() {
        return publicationDay;
    }

    public Integer getPublicationYear() {
        return publicationYear;
    }
}
