package nl.wendy.boeken.api.searching;

import nl.wendy.boeken.api.books.Book;
import org.json.JSONObject;
import org.json.XML;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * Manages goodreads searches
 */
public class SearchManager {

    /**
     * performs search with the provided book as basis
     * @param book the (incomplete) book to search for
     * @return two searchresults, one for a title search and one for a full search
     * @throws Exception if something goes wrong
     */
    public ArrayList<SearchResult> performSearch(Book book) throws Exception {
        URLManager urlManager;
        urlManager = new URLManager();
        String urlFull;
        String urlTitle;
        if (book.getTitle() != null) {
            if (book.getSeries() != null) {
                if (book.getNumberInSeries() != null) {
                    urlFull = urlManager.getFormattedURL(book.getAuthor().getFullName(), book.getSeries().getName() + " " +
                            book.getNumberInSeries() + " - " + book.getTitle(), true);
                    urlTitle = urlManager.getFormattedURL(book.getAuthor().getFullName(), book.getSeries().getName() + " " +
                            book.getNumberInSeries() + " - " + book.getTitle(), false);
                } else {
                    urlFull = urlManager.getFormattedURL(book.getAuthor().getFullName(), book.getSeries().getName() +
                            " - " + book.getTitle(), true);
                    urlTitle = urlManager.getFormattedURL(book.getAuthor().getFullName(), book.getSeries().getName() +
                            " - " + book.getTitle(), false);
                }
            } else {
                urlFull = urlManager.getFormattedURL(book.getAuthor().getFullName(), book.getTitle(), true);
                urlTitle = urlManager.getFormattedURL(book.getAuthor().getFullName(), book.getTitle(), false);
            }
        } else {
            urlFull = urlManager.getFormattedURL(book.getAuthor().getFullName(), book.getSeries().getName() + " " +
                    book.getNumberInSeries(), true);
            urlTitle = urlManager.getFormattedURL(book.getAuthor().getFullName(), book.getSeries().getName() + " " +
                    book.getNumberInSeries(), false);
        }

        JSONObject outputTitle  = getUrlContents(urlTitle);
        JSONObject outputFull  = getUrlContents(urlFull);
        ArrayList<SearchResult> out = new ArrayList<>(2);
        if (outputTitle != null) {
            SearchResult titleSearch = new SearchResult("title_search", outputTitle);
            out.add(titleSearch);
        } else {
            out.add(null);
        }
        if(outputFull != null) {
            SearchResult fullSearch = new SearchResult("full_search", outputFull);
            out.add(fullSearch);
        } else {
            out.add(null);
        }

        return out;
    }

    /**
     * collects all the data goodreads has for the url provided
     * @param theUrl the url from which data has to be collected
     * @return a json representation of the response
     * @throws Exception if something goes wrong
     */
    private static JSONObject getUrlContents(String theUrl) throws Exception {
        StringBuilder content = new StringBuilder();

        // many of these calls can throw exceptions, so i've just
        // wrapped them all in one try/catch statement.
        URL url = new URL(theUrl);

        // create a url connection object
        URLConnection urlConnection = url.openConnection();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

        String line;

        // read from the url connection via the buffered reader
        while ((line = bufferedReader.readLine()) != null)
        {
            content.append(line).append("\n");
        }
        bufferedReader.close();

        JSONObject xmlJSONObj = XML.toJSONObject(content.toString());
        return xmlJSONObj.getJSONObject("GoodreadsResponse");
    }
}
