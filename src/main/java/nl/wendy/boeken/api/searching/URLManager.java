package nl.wendy.boeken.api.searching;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Manages url construction
 */
public class URLManager {
    private final Properties connectionProperties;

    /**
     * constructor, private to be able to use singleton pattern
     * @throws IOException if reading of database properties goes wrong
     */
    URLManager() throws IOException {
        try {
            ClassLoader cl = Thread.currentThread().getContextClassLoader();
            InputStream in = cl.getResourceAsStream("goodReadsCreds.properties");
            this.connectionProperties = new Properties();
            this.connectionProperties.load(in);
            in.close();
        } catch (IOException ex) {
            throw new IOException("URLManager - IOException: " + ex.getMessage());
        }
    }

    /**
     * stores the base url for title search
     * @return the url
     */
    private String getTitleSearchURL() {
        return String.format("https://www.goodreads.com/book/title.xml?key=%s", this.connectionProperties.getProperty("key"));
    }

    /**
     * stores the base url for full search
     * @return the url
     */
    private String getFullSearchURL() {
        return String.format("https://www.goodreads.com/search/index.xml?key=%s&q=", this.connectionProperties.getProperty("key"));
    }

    /**
     * constructs an url specific for data from the book described in the parameters
     * @param author the author of the book
     * @param title the title of the book
     * @param fullSearch whether to do a full search or a title search
     * @return the url for that book
     */
    public String getFormattedURL(String author, String title, boolean fullSearch) {
        if (fullSearch) {
            String baseURL = getFullSearchURL();
            return baseURL + author.replaceAll(" ", "+") + "%20" + title.replaceAll(" ", "+");
        } else {
            String toFormat = getTitleSearchURL() + "&title=%s";
            if (author != null) {
                toFormat = toFormat + "&author=%s";
                return String.format(toFormat, title.replaceAll(" ", "+"), author.replaceAll(" ", "+"));
            }

            return String.format(toFormat, title);
        }
    }
}
