package nl.wendy.boeken.api.servlets;

import nl.wendy.boeken.api.agenda.AgendaManager;
import nl.wendy.boeken.api.book_management.BooksManager;
import nl.wendy.boeken.api.book_management.NotReleasedManager;
import nl.wendy.boeken.api.books.NotReleasedBook;
import nl.wendy.boeken.api.searching.SearchResult;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@WebServlet(name = "PerformanceServlet", urlPatterns = "/perform.tasks")
public class PerformanceServlet extends HttpServlet {

    /**
     * Manages post requests
     * @param request request object
     * @param response response object
     * @throws ServletException, if something with the servlet goes wrong
     * @throws IOException, if file handling goes wrong.
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        NotReleasedManager manager = (NotReleasedManager) session.getAttribute("notReleasedManager");;
        String page = request.getParameter("page");
        RequestDispatcher view = request.getRequestDispatcher(page + ".jsp");
        int bookId = Integer.parseInt(request.getParameter("book"));
        NotReleasedBook book = manager.getNotReleasedBooks().get(bookId);
        String wayOfUpdate = request.getParameter("way_of_update");
        if (wayOfUpdate.equals("result")) {
            String result = request.getParameter("search_result");
            ArrayList<SearchResult> searchResults = manager.getLogger().getSearchResults().get(book);
            SearchResult searchResult;
            if (result.equals("full")) {
                searchResult = searchResults.get(1);
            } else {
                searchResult = searchResults.get(0);
            }
            boolean success = manager.updateRecord(book, searchResult, request.getParameterMap().get("task"));
            request.setAttribute("book", bookId);
            if (success) {
                request.removeAttribute("error");
                request.setAttribute("success", true);
            } else {
                request.removeAttribute("success");
                request.setAttribute("error", true);
            }
        } else {
            String customString = request.getParameter("custom_result");
            String[] tasks = customString.split(",");
            HashMap<String, String> args = new HashMap<>(tasks.length);
            if (Integer.parseInt(request.getParameter("title_idx")) > 0) {
                args.put("TITEL", tasks[Integer.parseInt(request.getParameter("title_idx"))-1].trim());
            }
            if (Integer.parseInt(request.getParameter("release_idx")) > 0) {
                args.put("DATUM_UITKOMST", tasks[Integer.parseInt(request.getParameter("release_idx"))-1].trim());
            }

            boolean success = manager.updateRecord(book, args);
            request.setAttribute("book", bookId);
            if (success) {
                request.removeAttribute("error");
                request.setAttribute("success", true);
            } else {
                request.removeAttribute("success");
                request.setAttribute("error", true);
            }
        }
        session.setAttribute("notReleasedManager", manager);

        boolean addToAgenda = Boolean.parseBoolean(request.getParameter("agenda"));
        if (addToAgenda) {
            try {
                AgendaManager agendaManager = new AgendaManager();
                agendaManager.addToAgenda(book);
            } catch (GeneralSecurityException e) {
                request.removeAttribute("success");
                request.setAttribute("error", true);
            }
        }

        ServletContext servletContext = getServletContext();
        String filePath = servletContext.getRealPath(servletContext.getInitParameter("logFile"));
        manager.getLogger().writeErrorsToFile(filePath);
        request.setAttribute("logFilePath", filePath);

        view.forward(request, response);
    }

    /**
     * Manages get requests
     * @param request request object
     * @param response response object
     * @throws ServletException, if something with the servlet goes wrong
     * @throws IOException, if file handling goes wrong.
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String[] tasks = request.getParameterMap().get("task");
        HttpSession session = request.getSession(true);
        List<String> tasksList = Arrays.asList(tasks);
        NotReleasedManager manager = null;
        if (tasksList.contains("moveNewRel") || tasksList.contains("searchNotRel")) {
            manager = new NotReleasedManager();
            if (tasksList.contains("moveNewRel")) {
                manager.updateReleasedBooks();
            }
            if (tasksList.contains("searchNotRel")) {
                manager.searchForInfoUpdates();
            }
            session.setAttribute("notReleasedManager", manager);
        }

        // write possible errors to file
        if (manager != null) {
            ServletContext servletContext = getServletContext();
            String filePath = servletContext.getRealPath(servletContext.getInitParameter("logFile"));
            manager.getLogger().writeErrorsToFile(filePath);
            request.setAttribute("logFilePath", filePath);
        }

        request.setAttribute("success", true);
        RequestDispatcher view = request.getRequestDispatcher("index.jsp");
        view.forward(request, response);
    }
}
