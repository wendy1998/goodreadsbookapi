package nl.wendy.boeken.api.book_management;

import nl.wendy.boeken.api.books.Book;
import nl.wendy.boeken.api.books.NotReleasedBook;
import nl.wendy.boeken.api.books.WantedBook;
import nl.wendy.boeken.api.dao.ConnectionManager;
import nl.wendy.boeken.api.exceptions.DatabaseException;
import nl.wendy.boeken.api.logging.NotReleasedLogger;
import nl.wendy.boeken.api.model.Author;
import nl.wendy.boeken.api.model.Series;
import nl.wendy.boeken.api.searching.SearchManager;
import nl.wendy.boeken.api.searching.SearchResult;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static nl.wendy.boeken.api.dao.DatabaseConnectable.cleanUp;

/**
 * manages everything having to do with not released books
 */
public class NotReleasedManager extends BooksManager {
    /**
     * constructor
     */
    public NotReleasedManager() {
        this.logger = new NotReleasedLogger();
        try {
            this.loadBooks();
        } catch (DatabaseException e) {
            this.logger.logException(e);
        }
    }

    /**
     * update a record with new data from a search result
     * @param book the book to be modified
     * @param searchResult the search result containing the new data
     * @param tasks what to change
     * @return whether it was successful
     */
    public boolean updateRecord(Book book, SearchResult searchResult, String ... tasks) {
        String sql = "update boeken_die_nog_niet_uit_zijn set";
        boolean needUpdate = false;
        try {
            NotReleasedBook notReleasedBook = (NotReleasedBook) book;
            for (String task : tasks) {
                if (task.equals("releaseDate")) {
                    String releaseDate = notReleasedBook.constructReleaseDate(
                            searchResult.getPublicationYear(),
                            searchResult.getPublicationMonth(),
                            searchResult.getPublicationDay()
                    );
                    notReleasedBook.setReleaseDate(releaseDate);
                    if (notReleasedBook.getReleaseDate() != null) {
                        if (sql.contains("TITEL")) {
                            sql = sql + ", DATUM_UITKOMST = '" + notReleasedBook.getReleaseDate() + "'";
                        } else {
                            sql = sql + " DATUM_UITKOMST = '" + notReleasedBook.getReleaseDate() + "'";
                        }
                        needUpdate = true;
                    }
                }
                if (task.equals("title")) {
                    notReleasedBook.setTitle(searchResult.getTitle());
                    if (notReleasedBook.getTitle() != null) {
                        if (sql.contains("DATUM_UITKOMST")) {
                            sql = sql + ", TITEL = '" + notReleasedBook.getTitle() + "'";
                        } else {
                            sql = sql + " TITEL = '" + notReleasedBook.getTitle() + "'";
                        }
                        needUpdate = true;
                    }
                }
            }
            sql = sql + " where ID = " + notReleasedBook.getTableId();

            if (needUpdate) {
                int numRowsAffected = ConnectionManager.defaultManager().executeUpdateStatement(sql);
                return numRowsAffected > 0;
            } else {
                return true;
            }
        } catch (Exception e) {
            this.logger.logException(e);
        }
        return false;
    }

    /**
     * update a record with new data from a search result
     * @param book the book to be modified
     * @param args key value pairs of fields in the database and their value that need to be updated.
     * @return whether it was successful
     */
    public boolean updateRecord(Book book, HashMap<String, String> args) {
        String sql = "update boeken_die_nog_niet_uit_zijn set";
        NotReleasedBook notReleasedBook = (NotReleasedBook) book;
        boolean first = true;
        for (Map.Entry<String, String> entry: args.entrySet()) {
            if (first) {
                sql = sql + String.format(" %s = '%s'", entry.getKey(), entry.getValue());
                first = false;
            } else {
                sql = sql + String.format(", %s = '%s'", entry.getKey(), entry.getValue());
            }
        }
        sql = sql + " where ID = " + notReleasedBook.getTableId();

        try {
            int numRowsAffected = ConnectionManager.defaultManager().executeUpdateStatement(sql);
            if(numRowsAffected > 0) {
                notReleasedBooks.remove(notReleasedBook.getTableId());
                this.loadBooks();
                return true;
            }
        } catch (Exception e) {
            this.logger.logException(e);
        }
        return false;
    }

    // conversions

    /**
     * convert a not released book into a book I have
     * @param toReleaseBook the to be converted book
     * @throws Exception if something goes wrong
     */
    public void turnNotReleasedIntoOwned(NotReleasedBook toReleaseBook) throws Exception {
        turnNotReleasedIntoWanted(toReleaseBook);
        WantedBook newBook = (WantedBook) this.logger.getConversions().get("not_released").get(toReleaseBook);
        if (newBook != null) {
            ReleasedManager releasedManager = new ReleasedManager();
            releasedManager.turnWantedIntoOwned(newBook);
        }
    }

    /**
     * convert a not released book into a book I have
     * @param toReleaseBook the to be converted book
     * @param novel68 whether it was found on novel68
     * @param stillCalibre whether it has to be added to calibre
     * @param stillEReader whether it has to be added to my ereader
     * @param stillCollection whether it has to be added to a collection
     * @throws Exception if something goes wrong
     */
    public void turnNotReleasedIntoOwned(NotReleasedBook toReleaseBook,
                                  boolean novel68,
                                  boolean stillCalibre,
                                  boolean stillEReader,
                                  boolean stillCollection) throws Exception {
        turnNotReleasedIntoWanted(toReleaseBook);
        WantedBook newBook = (WantedBook) this.logger.getConversions().get("not_released").get(toReleaseBook);
        if (newBook != null) {
            ReleasedManager releasedManager = new ReleasedManager();
            releasedManager.turnWantedIntoOwned(newBook, novel68, stillCalibre, stillEReader, stillCollection);
        }
    }

    /**
     * convert a not released book into a book I have
     * @param toReleaseBook the to be converted book
     * @param novel68 whether it was found on novel68
     * @param stillCalibre whether it has to be added to calibre
     * @param stillEReader whether it has to be added to my ereader
     * @param stillCollection whether it has to be added to a collection
     * @param read whether I have read the book
     * @param bookType ebook or book
     * @throws Exception if something goes wrong
     */
    public void turnNotReleasedIntoOwned(NotReleasedBook toReleaseBook,
                                  boolean novel68,
                                  boolean stillCalibre,
                                  boolean stillEReader,
                                  boolean stillCollection,
                                  boolean read,
                                  String bookType) throws Exception {
        turnNotReleasedIntoWanted(toReleaseBook);
        WantedBook newBook = (WantedBook) this.logger.getConversions().get("not_released").get(toReleaseBook);
        if (newBook != null) {
            ReleasedManager releasedManager = new ReleasedManager();
            releasedManager.turnWantedIntoOwned(newBook, novel68, stillCalibre, stillEReader, stillCollection, read, bookType);
        }
    }

    /**
     * turns a wanted book back into a not released book if a conversion failed
     * @param wantedBookId the id of the wanted book to be turned back
     * @param original the book it was initially
     * @throws DatabaseException if database connectivity goes wrong
     */
    public void turnWantedIntoNotReleased(int wantedBookId, NotReleasedBook original) throws DatabaseException {
        turnWantedIntoNotReleased(wantedBookId, original.getReleaseDate(), original.isStillAgenda());
    }

    /**
     * turns a wanted book into a not released book if a mistake was made
     * @param wantedBookId the id of the wanted book to be turned back
     * @param releaseDate the date the non-released book comes out
     * @param stillAgenda whether it needs to be added to the agenda
     * @throws DatabaseException if database connectivity goes wrong
     */
    public void turnWantedIntoNotReleased(int wantedBookId, String releaseDate, boolean stillAgenda) throws DatabaseException {
        CallableStatement call = null;
        Connection conn = ConnectionManager.defaultManager().getConnection();
        String sql = "call verplaats_wil_ik_naar_niet_uit(?, ?, ?)";
        try {
            call = conn.prepareCall(sql);
            call.setString(1, releaseDate);
            call.setBoolean(2, stillAgenda);
            call.setInt(3, wantedBookId);
            call.execute();
        } catch (SQLException e) {
            try {
                conn.close();
            } catch (SQLException ex) {
                throw new DatabaseException("NotReleasedManager: turnWantedIntoNotReleased - SQLException: " + e.getMessage());
            }
            throw new DatabaseException("NotReleasedManager: turnWantedIntoNotReleased - SQLException: " + e.getMessage());
        } finally {
            cleanUp(call, null, true);
        }
    }

    /**
     * Turns a not released book into a wanted book
     * @param toReleaseBook the book to be converted
     * @throws Exception if something goes wrong
     */
    public void turnNotReleasedIntoWanted(NotReleasedBook toReleaseBook) throws Exception {
        // move to wanted table
        CallableStatement call = null;
        Connection conn = ConnectionManager.defaultManager().getConnection();
        String sql = "call verplaats_naar_wil_ik_met_id(?)";
        try {
            call = conn.prepareCall(sql);
            call.setInt(1, toReleaseBook.getTableId());
            call.execute();
        } catch (Exception e) {
            conn.close();
            this.logger.logException(e);
        } finally {
            cleanUp(call, null, false);
        }

        PreparedStatement stmt = null;
        ResultSet rs = null;
        sql = "select * from boeken_die_ik_wil where ID = (select max(ID) from boeken_die_ik_wil)";
        int id = 0;
        try {
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            rs.next();

            // collect info for new wanted book from table
            id = rs.getInt(1);
            Author author = new Author(rs.getInt(2));
            Series series = null;
            if (rs.getObject(3) != null) {
                series = new Series(rs.getInt(3));
            }
            String numberInSeries = null;
            if (series != null && rs.getObject(4) != null) {
                numberInSeries = rs.getString(4);
            }
            String title = rs.getString(5);
            String language = rs.getString(6);

            // add info to new object and add that to hashmap with wanted books.
            WantedBook wantedBook = new WantedBook(author, series, numberInSeries, title, language, id);

            // updating hashmaps and logging
            wantedBooks.put(id, wantedBook);
            notReleasedBooks.remove(toReleaseBook.getTableId());
            this.logger.logConversion(toReleaseBook, wantedBook);
        } catch (Exception e) {
            this.logger.logConversion(toReleaseBook, null);
            turnWantedIntoNotReleased(id, toReleaseBook);
            updateBookId(toReleaseBook);
            this.logger.logException(e);
        } finally {
            cleanUp(stmt, rs, true);
        }
    }

    /**
     * Update the book if of a not released book book because it was turned back
     * @param original the not released book of which the id needs to be changed
     * @throws DatabaseException if database connectivity goes wrong
     */
    public void updateBookId(NotReleasedBook original) throws DatabaseException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String sql = "select * from boeken_die_nog_niet_uit_zijn where ID = (select max(ID) from boeken_die_nog_niet_uit_zijn)";
        try {
            Connection conn = ConnectionManager.defaultManager().getConnection();
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            rs.next();

            // collect info for new wanted book from table
            int newId = rs.getInt(1);
            this.getNotReleasedBooks().remove(original.getTableId());
            original.setTableId(newId);
            this.getNotReleasedBooks().put(newId, original);
        } catch (SQLException e) {
            throw new DatabaseException("NotReleasedManager: updateBookId - SQLException: " + e.getMessage());
        } finally {
            cleanUp(stmt, rs, true);
        }
    }

    /**
     * Check if there are loaded books of which the release date has passed and turn them into wanted books
     */
    public void updateReleasedBooks() {
        try {
            // cannot directly call turn not released into wanted because then hashmap
            // where the books come from is modified while looping over it
            ArrayList<NotReleasedBook> toRelease = new ArrayList<>();
            for (NotReleasedBook book : notReleasedBooks.values()) {
                if (book.wasReleased()) {
                    toRelease.add(book);
                }
            }

            for (NotReleasedBook book : toRelease) {
                turnNotReleasedIntoWanted(book);
            }
        } catch (Exception e) {
            this.logger.logException(e);
        }
    }

    /**
     * Load all unreleased books from the database
     * @throws DatabaseException if database connectivity goes wrong
     */
    private void loadBooks() throws DatabaseException {
        ResultSet rs = null;
        PreparedStatement stmt = null;
        try {
            Connection conn = ConnectionManager.defaultManager().getConnection();
            String sql = "select * from boeken_die_nog_niet_uit_zijn;";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            NotReleasedBook book;
            while (rs.next()) {
                int id = rs.getInt(1);
                Author author = new Author(rs.getInt(2));
                String releaseDate = rs.getString(3);
                Series series = null;
                if (rs.getObject(4) != null) {
                    series = new Series(rs.getInt(4));
                }
                String numberInSeries = null;
                if (series != null && rs.getObject(5) != null) {
                    numberInSeries = rs.getString(5);
                }
                String title = rs.getString(6);
                String language = rs.getString(7);
                boolean stillAgenda = rs.getBoolean(8);

                book = new NotReleasedBook(author, series, numberInSeries, title, language, releaseDate, stillAgenda, id);
                notReleasedBooks.putIfAbsent(book.getTableId(), book);
            }
        } catch (Exception e) {
            this.logger.logException(e);
        } finally {
            cleanUp(stmt, rs, true);
        }
    }

    /**
     * Search for new info on loaded books
     */
    public void searchForInfoUpdates() {
        try {
            SearchManager searchManager = new SearchManager();
            NotReleasedLogger logger = (NotReleasedLogger) this.logger;
            HashMap<NotReleasedBook, ArrayList<SearchResult>> searchesLog = logger.getSearchResults();
            for (NotReleasedBook book: notReleasedBooks.values()) {
                try {
                    ArrayList<SearchResult> searchResults = searchManager.performSearch(book);
                    searchesLog.put(book, searchResults);
                } catch (Exception e) {
                    this.logger.logException(e);
                }
            }
        } catch (Exception e) {
            this.logger.logException(e);
        }
    }

    // getters
    @Override
    public NotReleasedLogger getLogger() {
        return (NotReleasedLogger) this.logger;
    }
}
