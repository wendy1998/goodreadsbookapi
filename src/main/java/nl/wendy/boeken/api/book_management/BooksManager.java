package nl.wendy.boeken.api.book_management;

import nl.wendy.boeken.api.books.NotReleasedBook;
import nl.wendy.boeken.api.books.OwnedBook;
import nl.wendy.boeken.api.books.WantedBook;
import nl.wendy.boeken.api.logging.Logger;

import java.util.HashMap;

/**
 * Manages all books and stores them
 */
public abstract class BooksManager {
    protected static HashMap<Integer, OwnedBook> newBooks = new HashMap<>();
    protected static HashMap<Integer, WantedBook> wantedBooks = new HashMap<>();
    protected static HashMap<Integer, NotReleasedBook> notReleasedBooks = new HashMap<>();
    Logger logger;

    // getters
    public HashMap<Integer, OwnedBook> getNewBooks() {
        return newBooks;
    }

    public HashMap<Integer, WantedBook> getWantedBooks() {
        return wantedBooks;
    }

    public HashMap<Integer, NotReleasedBook> getNotReleasedBooks() {
        return notReleasedBooks;
    }

    public Logger getLogger() {
        return logger;
    }
}
