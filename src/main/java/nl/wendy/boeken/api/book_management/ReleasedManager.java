package nl.wendy.boeken.api.book_management;

import nl.wendy.boeken.api.books.OwnedBook;
import nl.wendy.boeken.api.books.WantedBook;
import nl.wendy.boeken.api.dao.ConnectionManager;
import nl.wendy.boeken.api.exceptions.DatabaseException;
import nl.wendy.boeken.api.logging.Logger;
import nl.wendy.boeken.api.model.Author;
import nl.wendy.boeken.api.model.Series;

import java.sql.*;

import static nl.wendy.boeken.api.dao.DatabaseConnectable.cleanUp;

/**
 * Manager of actions with released books
 */
public class ReleasedManager extends BooksManager {

    /**
     * constructor
     */
    public ReleasedManager() {
        this.logger = new Logger("released_logger");
        try {
            this.loadBooks();
        } catch (DatabaseException e) {
            this.logger.logException(e);
        }
    }

    /**
     * turns a book I thought I had into a book I want
     * @param ownedBookId the id of the book I thought I had in the database
     * @param hopeless whether it is hopeless to look for the book
     * @throws DatabaseException if database connectivity goes wrong
     */
    void turnOwnedIntoWanted(int ownedBookId, boolean hopeless) throws DatabaseException {
        CallableStatement call = null;
        Connection conn = ConnectionManager.defaultManager().getConnection();
        String sql = "call verplaats_heb_ik_naar_wil_ik(?, ?)";
        try {
            call = conn.prepareCall(sql);
            call.setInt(1, ownedBookId);
            call.setBoolean(2, hopeless);
            call.execute();
        } catch (SQLException e) {
            throw new DatabaseException("ReleasedManager: turnOwnedIntoWanted - SQLException: " + e.getMessage());
        } finally {
            cleanUp(call, null, true);
        }
    }

    /**
     * Update the book if of a wanted book because it was turned back
     * @param original the wanted book of which the id needs to be changed
     * @throws DatabaseException if database connectivity goes wrong
     */
    void updateBookId(WantedBook original) throws DatabaseException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String sql = "select * from boeken_die_ik_wil where ID = (select max(ID) from boeken_die_ik_wil)";
        try {
            Connection conn = ConnectionManager.defaultManager().getConnection();
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            rs.next();

            // collect info for new wanted book from table
            int newId = rs.getInt(1);
            this.getWantedBooks().remove(original.getTableId());
            original.setTableId(newId);
            this.getWantedBooks().put(newId, original);
        } catch (SQLException e) {
            throw new DatabaseException("ReleasedManager: updateBookId - SQLException: " + e.getMessage());
        } finally {
            cleanUp(stmt, rs, true);
        }
    }

    /**
     * converts a wanted book into an owned book
     * @param book the book to convert
     * @param novel68 whether it was found on novel68
     * @param stillCalibre whether it has to be added to calibre
     * @param stillEReader whether it has to be added to my ereader
     * @param stillCollection whether it has to be added to a collection
     * @throws DatabaseException if database connectivity goes wrong
     */
    void turnWantedIntoOwned(WantedBook book,
                             boolean novel68,
                             boolean stillCalibre,
                             boolean stillEReader,
                             boolean stillCollection) throws DatabaseException {
        turnWantedIntoOwned(book, novel68,
                stillCalibre, stillEReader,
                stillCollection, false, "Ebook");
    }

    /**
     * converts a wanted book into an owned book
     * @param book the book to convert
     * @throws DatabaseException if database connectivity goes wrong
     */
    void turnWantedIntoOwned(WantedBook book) throws DatabaseException {
        turnWantedIntoOwned(book, false, true, true,
                true, false, "Ebook");
    }

    /**
     * converts a wanted book into an owned book
     * @param book the book to convert
     * @param novel68 whether it was found on novel68
     * @param stillCalibre whether it has to be added to calibre
     * @param stillEReader whether it has to be added to my ereader
     * @param stillCollection whether it has to be added to a collection
     * @param read whether I have read the book
     * @param bookType ebook or book
     * @throws DatabaseException if database connectivity goes wrong
     */
    void turnWantedIntoOwned(WantedBook book,
                             boolean novel68,
                             boolean stillCalibre,
                             boolean stillEReader,
                             boolean stillCollection,
                             boolean read,
                             String bookType) throws DatabaseException {
        // call stored procedure in database to move the book to correct table
        CallableStatement call = null;
        Connection conn = ConnectionManager.defaultManager().getConnection();
        String sql = "call verplaats_wil_ik_naar_mijn_boeken(?, ?, ?, ?, ?, ?, ?)";
        try {
            call = conn.prepareCall(sql);
            call.setInt(1, book.getTableId());
            call.setBoolean(2, stillCalibre);
            call.setBoolean(3, stillEReader);
            call.setBoolean(4, stillCollection);
            call.setBoolean(5, read);
            call.setString(6, bookType);
            call.setBoolean(7, novel68);
            call.execute();
        } catch (SQLException e) {
            try {
                conn.close();
            } catch (SQLException ex) {
                throw new DatabaseException("ReleasedManager: turnWantedIntoOwned - SQLException: " + e.getMessage());
            }
            throw new DatabaseException("ReleasedManager: turnWantedIntoOwned - SQLException: " + e.getMessage());
        } finally {
            cleanUp(call, null, false);
        }

        // extract new book id
        PreparedStatement stmt = null;
        ResultSet rs = null;
        sql = "select * from mijn_boeken where BOEKNR = (select max(BOEKNR) from mijn_boeken)";
        int id = 0;
        try {
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            rs.next();

            // collect info for new wanted book from table
            id = rs.getInt(1);
            Author author = new Author(rs.getInt(2));
            String title = rs.getString(3);
            String language = rs.getString(4);
            Series series = null;
            if (rs.getObject(7) != null) {
                series = new Series(rs.getInt(7));
            }
            String numberInSeries = null;
            if (series != null && rs.getObject(8) != null) {
                numberInSeries = rs.getString(8);
            }

            // create new ownedBook object
            OwnedBook ownedBook = new OwnedBook(author, series, numberInSeries, title, language, id, stillCalibre,
                    stillEReader, stillCollection, novel68, bookType);

            // remove old book from hashmap with wanted books
            wantedBooks.remove(book.getTableId());
            // add new OwnedBook to hashmap using parameters and new book id
            newBooks.put(id, ownedBook);
            this.logger.logConversion(book, ownedBook);
        } catch (SQLException e) {
            this.logger.logConversion(book, null);
            turnOwnedIntoWanted(id, book.isHopeless());
            updateBookId(book);
            throw new DatabaseException("ReleasedManager: turnWantedIntoOwned - SQLException: " + e.getMessage());
        } catch (Exception e) {
            this.logger.logConversion(book, null);
            turnOwnedIntoWanted(id, book.isHopeless());
            updateBookId(book);
            throw e;
        } finally {
            cleanUp(stmt, rs, true);
        }
    }

    /**
     * Loads all the wanted books from the database
     * @throws DatabaseException if database connectivity goes wrong
     */
    void loadBooks() throws DatabaseException {
        ResultSet rs = null;
        PreparedStatement stmt = null;
        try {
            Connection conn = ConnectionManager.defaultManager().getConnection();
            String sql = "select * from boeken_die_ik_wil;";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            WantedBook book;
            while (rs.next()) {
                int id = rs.getInt(1);
                Author author = new Author(rs.getInt(2));
                Series series = null;
                if (rs.getObject(3) != null) {
                    series = new Series(rs.getInt(3));
                }
                String numberInSeries = null;
                if (series != null && rs.getObject(4) != null) {
                    numberInSeries = rs.getString(4);
                }
                String title = rs.getString(5);
                String language = rs.getString(6);
                String dateAdded = rs.getString(7);
                boolean hopeless = rs.getBoolean(8);

                book = new WantedBook(author, series, numberInSeries, title, language, id, dateAdded, hopeless);
                wantedBooks.putIfAbsent(book.getTableId(), book);
            }
        } catch (Exception e) {
            this.logger.logException(e);
        } finally {
            cleanUp(stmt, rs, true);
        }
    }
}
