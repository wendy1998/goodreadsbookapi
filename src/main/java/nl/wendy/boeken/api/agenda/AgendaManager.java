package nl.wendy.boeken.api.agenda;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.services.calendar.model.EventReminder;
import nl.wendy.boeken.api.books.NotReleasedBook;
import nl.wendy.boeken.api.dao.ConnectionManager;
import nl.wendy.boeken.api.exceptions.DatabaseException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import static nl.wendy.boeken.api.dao.DatabaseConnectable.cleanUp;

/**
 * Manages all things having to do with the agenda
 */
public class AgendaManager {
    private static final String APPLICATION_NAME = "Library Api";
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final String TOKENS_DIRECTORY_PATH = "tokens";
    private Calendar agenda;

    /**
     * Global instance of the scopes required by this quickstart.
     * If modifying these scopes, delete your previously saved tokens/ folder.
     */
    private static final List<String> SCOPES = Collections.singletonList(CalendarScopes.CALENDAR);
    private static final String CREDENTIALS_FILE_PATH = "/credentials.json";

    public AgendaManager() throws GeneralSecurityException, IOException {
        this.agenda = setCalender();
    }

    /**
     * Creates an authorized Credential object.
     * @param HTTP_TRANSPORT The network HTTP Transport.
     * @return An authorized Credential object.
     * @throws IOException If the credentials.json file cannot be found.
     */
    private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
        // Load client secrets.
        InputStream in = AgendaManager.class.getResourceAsStream(CREDENTIALS_FILE_PATH);
        if (in == null) {
            throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
        }
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("offline")
                .build();
        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
        return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
    }

    /**
     * Specifies a calendar
     * @return the calendar
     * @throws GeneralSecurityException if connecting to the calendar goes wrong
     * @throws IOException if property reading goes wrong
     */
    private static Calendar setCalender() throws GeneralSecurityException, IOException {
        // Build a new authorized API client service.
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        return new Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                .setApplicationName(APPLICATION_NAME)
                .build();
    }

    /**
     * Add all the books in the table 'nog_agenda' to the agenda
     * @throws DatabaseException if database connectivity goes wrong
     * @throws IOException if file reading goes wrong
     * @throws SQLException if cleanup goes wrong
     */
    public void addToAgenda() throws DatabaseException, IOException, SQLException {
        Connection conn = ConnectionManager.defaultManager().getConnection();
        String sql = "SELECT * FROM `nog agenda`;";
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ArrayList<HashMap<String, String>> toAdd = new ArrayList<>();

        try {
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            while (rs.next()) {
                HashMap<String, String> entry = new HashMap<>(4);
                entry.put("title", rs.getString("TITEL"));
                entry.put("numberInSr", rs.getString("NUMMER_IN_SERIE"));
                entry.put("series", rs.getString("SERIE"));
                entry.put("release", rs.getString("DATUM_UITKOMST"));

                toAdd.add(entry);
            }
        } catch (SQLException e) {
            cleanUp(stmt, rs, true);
            throw new DatabaseException("AgendaManager: addToAgenda - SQLException: " + e.getMessage());
        } finally {
            if (!conn.isClosed()) {
                cleanUp(stmt, rs, false);
            }
        }

        // add event to agenda
        for (HashMap<String, String> entry : toAdd) {
            Event event = new Event()
                    .setSummary(String.format("%s %s - %s uit!!!",
                            entry.get("series"),
                            entry.get("numberInSr"),
                            entry.get("title")
                    ));

            DateTime startDateTime = new DateTime(entry.get("release"));
            EventDateTime start = new EventDateTime()
                    .setDate(startDateTime);
            event.setStart(start);

            DateTime endDateTime = new DateTime(entry.get("release"));
            EventDateTime end = new EventDateTime()
                    .setDate(endDateTime);
            event.setEnd(end);

            EventReminder[] reminderOverrides = new EventReminder[]{
                    new EventReminder().setMethod("popup").setMinutes(10),
            };
            Event.Reminders reminders = new Event.Reminders()
                    .setUseDefault(false)
                    .setOverrides(Arrays.asList(reminderOverrides));
            event.setReminders(reminders);

            String calendarId = "primary";
            event = this.agenda.events().insert(calendarId, event).execute();
            System.out.printf("Event created: %s\n", event.getHtmlLink());
        }

        try {
            sql = "UPDATE `boeken_die_nog_niet_uit_zijn` SET `NOG_AGENDA` = '0' WHERE NOG_AGENDA = 1;";
            stmt = conn.prepareStatement(sql);
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw new DatabaseException("AgendaManager: addToAgenda - SQLException: " + e.getMessage());
        } finally {
            cleanUp(stmt, null, true);
        }

    }

    /**
     * Add a specific book to the agenda
     * @param book the book to add
     * @throws IOException if adding to the agenda goes wrong
     */
    public void addToAgenda(NotReleasedBook book) throws IOException {
        // add event to agenda
        Event event = new Event();
        String toFormat = "%s - %s uit!!!";
        if (book.getSeries() != null && book.getNumberInSeries() != null) {
            toFormat = "%s " + toFormat;
            event.setSummary(String.format(toFormat, book.getSeries().getName(), book.getNumberInSeries(), book.getTitle()));
            if (book.getNumberInSeries() == null) {
                event.setSummary(String.format(toFormat, book.getSeries().getName(), book.getTitle()));
            }
        } else {
            event.setSummary(String.format(toFormat, book.getAuthor().getFullName(), book.getTitle()));
        }

        DateTime startDateTime = new DateTime(book.getReleaseDate());
        EventDateTime start = new EventDateTime()
                .setDate(startDateTime);
        event.setStart(start);

        DateTime endDateTime = new DateTime(book.getReleaseDate());
        EventDateTime end = new EventDateTime()
                .setDate(endDateTime);
        event.setEnd(end);

        EventReminder[] reminderOverrides = new EventReminder[]{
                new EventReminder().setMethod("popup").setMinutes(10),
        };
        Event.Reminders reminders = new Event.Reminders()
                .setUseDefault(false)
                .setOverrides(Arrays.asList(reminderOverrides));
        event.setReminders(reminders);

        String calendarId = "primary";
        event = this.agenda.events().insert(calendarId, event).execute();
        System.out.printf("Event created: %s\n", event.getHtmlLink());

    }
}
