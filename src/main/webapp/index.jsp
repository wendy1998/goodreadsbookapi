<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: wendy
  Date: 24-11-2019
  Time: 19:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Index</title>
    <jsp:include page="includes/head.jsp" />
  </head>
  <body>
  <div id="vue">
    <jsp:include page="includes/header.jsp">
      <jsp:param name="active" value="index"/>
    </jsp:include>
  </div>
  <c:if test="${requestScope.errorMessage != null}">
    <div class="container">
      <p class="red">${requestScope.errorMessage}</p>
    </div>
  </c:if>
  <c:if test="${requestScope.success == true}">
    <div class="container">
      <p class="green">Data successfully retrieved!</p>
    </div>
  </c:if>
  <div class="container">
    <form action="perform.tasks" method="get" style="display: inline-block">
      <label class="label">Move released books from not released table to wanted <input type="checkbox" value="moveNewRel" name="task"></label>
      <br/>
      <label class="label">Search for new info for not released books <input type="checkbox" value="searchNotRel" name="task"></label>
      <input type="submit" name="submit" value="submit" class="form-control">
    </form>
  </div>
  <jsp:include page="includes/footer.jsp" />
  <script src="scripts/javascript/vue/home.js" type="text/javascript"></script>
  </body>
</html>
