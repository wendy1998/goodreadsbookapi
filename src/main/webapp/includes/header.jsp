<%--
  Created by IntelliJ IDEA.
  User: wendy
  Date: 7-10-2019
  Time: 18:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<div class="row">
    <div class="col text-left">
        <div id='cssmenu'>
            <ul>
                <li v-for="item in menuItems"><a :href="item.page" :class="{'active': item.active}">{{ item.name }}</a></li>
            </ul>
        </div>
    </div>
    <div class="col text-center">
        <h1>{{ header }}</h1>
    </div>
    <div class="col text-right">

    </div>
</div>
</html>
