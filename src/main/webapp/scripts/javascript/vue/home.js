if (document.readyState) {
    new Vue( {
        el: '#vue',
        data: {
            header: 'Task request',
            menuItems: [
                {
                    name: 'Home',
                    page: 'index.jsp',
                    active: true
                },
                {
                    name: 'Not released',
                    page: 'not_released.jsp',
                    active: false
                }
            ]
        }
    });
}