if(document.readyState) {
    new Vue( {
        el: '#vue',
        data: {
            header: 'Search results for not released books',
            menuItems: [
                {
                    name: 'Home',
                    page: 'index.jsp',
                    active: false
                },
                {
                    name: 'Not released',
                    page: 'not_released.jsp',
                    active: true
                }
            ]
        }
    });
}