const manageVisibility = function(bookId) {
    const method = $("#updateSelector" + bookId + " :selected").val();
    const resultDiv = $("#result" + bookId);
    const customDiv = $("#custom" + bookId);

    if (method === "result") {
        if (resultDiv.hasClass("hidden")) {
            resultDiv.removeClass("hidden");
        }
        if (!customDiv.hasClass("hidden")) {
            customDiv.addClass("hidden");
        }
    } else {
        if (customDiv.hasClass("hidden")) {
            customDiv.removeClass("hidden");
        }
        if (!resultDiv.hasClass("hidden")) {
            resultDiv.addClass("hidden");
        }
    }
};

$(window).on("load", function () {
    const selectors = $(document.getElementsByClassName("updateSelector")).toArray();

    selectors.forEach(function (item) {
        let bookId = item.id.replace("updateSelector", "");
        item.addEventListener('change', function () {
            manageVisibility(bookId);
        });
        manageVisibility(bookId);
    })
});

