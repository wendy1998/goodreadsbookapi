<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: wendy
  Date: 27-1-2020
  Time: 16:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>not released task report</title>
    <jsp:include page="includes/head.jsp" />
</head>
<body>
<div id="vue">
    <jsp:include page="includes/header.jsp" />
</div>
<c:if test="${requestScope.errorMessage != null}">
    <div class="container">
        <p class="red">${requestScope.errorMessage}</p>
    </div>
</c:if>
<div class="container">
    <c:set var="actions_log" value="${sessionScope.notReleasedManager.logger}" scope="session" />
    <c:if test="${not empty actions_log.searchResults}">
        <c:forEach var="resultMapEntry" items="${actions_log.searchResults.entrySet()}">
            <c:set var="bookId" scope="page" value="${resultMapEntry.key.tableId}" />
            <div class="panel panel-default">
                <div class="panel-heading">
                        ${resultMapEntry.key} <i class="fa tip">&#xf05a; <span class="tooltip-text">
                        Release date: <c:choose>
                    <c:when test="${empty resultMapEntry.key.releaseDate}">
                        NA
                    </c:when>
                    <c:otherwise>
                        ${resultMapEntry.key.releaseDate}
                    </c:otherwise>
                </c:choose>
                    </span></i>
                </div>
                <div class="panel-body">
                    <!-- Nav tabs -->
                    <c:choose>
                        <c:when test="${resultMapEntry.value != null and
                        ((resultMapEntry.value[0] != null and resultMapEntry.value[0].found)
                        or
                        (resultMapEntry.value[1] != null and resultMapEntry.value[1].found))}">
                            <ul class="nav nav-tabs responsive">
                                <c:if test="${resultMapEntry.value[0] != null and resultMapEntry.value[0].found}">
                                    <li><a href="<c:out value="${'#title' += bookId}" />"
                                           class="active" data-toggle="tab"> Title search result </a>
                                    </li>
                                </c:if>
                                <c:if test="${resultMapEntry.value[1] != null and resultMapEntry.value[1].found}">
                                    <li><a href="<c:out value="${'#full' += bookId}" />"
                                           data-toggle="tab"> Full search result </a>
                                    </li>
                                </c:if>
                                <li><a href="<c:out value="${'#update' += bookId}" />"
                                       data-toggle="tab"> Update record </a>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content responsive">
                                <c:if test="${resultMapEntry.value[0] != null and resultMapEntry.value[0].found}">
                                    <div class="tab-pane fade in active"
                                         id="<c:out value="${'title' += bookId}" />">
                                        <c:set var="result" value="${resultMapEntry.value[0]}" scope="page" />
                                        <c:if test="${result.publicationYear != null}">
                                            <p>Release date: <br/>
                                                year: ${result.publicationYear}
                                                <c:if test="${result.publicationMonth != null}">
                                                    <br/>month: ${result.publicationMonth}
                                                </c:if>
                                                <c:if test="${result.publicationDay != null}">
                                                    <br/>day: ${result.publicationDay}
                                                </c:if>
                                            </p>
                                        </c:if>
                                        <c:if test="${result.title != null}">
                                            <p>Title: ${result.title}</p>
                                        </c:if>
                                    </div>
                                </c:if>
                                <c:if test="${resultMapEntry.value[1] != null and resultMapEntry.value[1].found}">
                                    <div class="tab-pane fade in" id="<c:out value="${'full' += bookId}" />">
                                        <c:set var="result" value="${resultMapEntry.value[1]}" scope="page" />
                                        <c:if test="${result.publicationYear != null}">
                                            <p>Release date: <br/>
                                                year: ${result.publicationYear}
                                                <c:if test="${result.publicationMonth != null}">
                                                    <br/>month: ${result.publicationMonth}
                                                </c:if>
                                                <c:if test="${result.publicationDay != null}">
                                                    <br/>day: ${result.publicationDay}
                                                </c:if>
                                            </p>
                                        </c:if>
                                        <c:if test="${result.title != null}">
                                            <p>Title: ${result.title}</p>
                                        </c:if>
                                    </div>
                                </c:if>
                                <div class="tab-pane fade in" id="<c:out value="${'update' += bookId}" />">
                                    <form action="perform.tasks" method="post">
                                        <input type="hidden" value="not_released" name="page">
                                        <input type="hidden" value="${bookId}" name="book">
                                        <label class="label">Way of update: <select name="way_of_update"
                                                                                    class="form-control updateSelector"
                                                                                    id="<c:out value="${'updateSelector' += bookId}" />">
                                            <option value="result" selected>Select a result</option>
                                            <option value="custom">Custom</option>
                                        </select></label>
                                        <div class="line">

                                        </div>
                                        <div id="<c:out value="${'custom' += bookId}" />">
                                            <label class="label form-control"><br/>Custom result:
                                                <input type="text" name="custom_result"
                                                       placeholder="read the tip"/><i class="fa tip">&#xf05a; <span class="tooltip-text">Separate the values<br/>
                                        by a comma and then<br/>
                                        choose the index<br/> of every task<br/> in the dropdown</span></i></label>
                                            <label class="label">Title <select name="title_idx" class="form-control">
                                                <option value="0">0</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                            </select></label>
                                            <label class="label">Release Date <select name="release_idx" class="form-control">
                                                <option value="0">0</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                            </select></label>
                                        </div>
                                        <div id="<c:out value="${'result' += bookId}" />">
                                            <div class="row">
                                                <div class="col">
                                                    <label class="label">Search result:
                                                        <select name="search_result" class="form-control">
                                                            <c:if test="${resultMapEntry.value[1] != null and resultMapEntry.value[1].found}">
                                                                <option value="full">Full search</option>
                                                            </c:if>
                                                            <c:if test="${resultMapEntry.value[0] != null and resultMapEntry.value[0].found}">
                                                                <option value="title">Title search</option>
                                                            </c:if>
                                                        </select></label>
                                                </div>
                                                <div class="col">
                                                    <p>Tasks:</p>
                                                    <label class="label">Title <input type="checkbox" value="title" name="task"></label>
                                                    <br/>
                                                    <label class="label">Release date <input type="checkbox" value="releaseDate" name="task"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <label class="label">Add to agenda: <select name="agenda" class="form-control">
                                            <option value="false" selected>no</option>
                                            <option value="true">yes</option>
                                        </select> </label>
                                        <input type="submit" value="submit" name="submit" class="form-control" />
                                    </form>
                                    <c:if test="${requestScope.success == true and requestScope.book == bookId}">
                                        <p class="green">Modification successful!</p>
                                    </c:if>
                                    <c:if test="${requestScope.error != null and requestScope.book == bookId}">
                                        <p class="red">Book couldn't be modified, see the
                                            <a href="download.do?filePath=${requestScope.logFilePath}&fileName=log.txt">log</a> file for details.</p>
                                    </c:if>
                                </div>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <p class="red">No search results found.</p>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="panel-footer"></div>
            </div>
        </c:forEach>
    </c:if>
    <jsp:include page="includes/footer.jsp" />
    <script src="scripts/javascript/manage_results_forms.js" type="text/javascript"></script>
    <script src="scripts/javascript/vue/not_released.js" type="text/javascript"></script>
</div>
</body>
</html>
